<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Dashboard route
 * @index
 */
Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

/**
 * Author resource routes
 * @index
 * @store
 * @create
 * @show
 * @update
 * @destroy
 * @edit
 */
Route::resource('authors', AuthorController::class);

/**
 * Book resource routes
 * @index
 * @store
 * @create
 * @show
 * @update
 * @destroy
 * @edit
 */
Route::resource('books', BookController::class);

Route::prefix('books')->group(function () {
    Route::get('/lend-book/{book}', [BookController::class, 'lendBook'])->name('books.lend');
    Route::get('/return-book/{book}', [BookController::class, 'returnBook'])->name('books.return');
});