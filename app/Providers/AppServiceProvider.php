<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->viewComposerAdminNavigationData();
    }

    /**
    * Add current route name to admin Navigation view
    */
    private function viewComposerAdminNavigationData()
    {
        view()->composer([
            'components.navbar',
        ], function ($view) {
            $view->with(['composerCurrentRouteName' => \Route::currentRouteName()]);
        });
    }

}
