<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;
use App\Http\Requests\StoreAuthor;
use App\Http\Requests\UpdateAuthor;
use Illuminate\Support\Collection;


class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [
            'title' => 'Zoznam autorov',
            'description' => 'Zoznam autorov',
            'authors' => $this->sortedAuthors($request),
        ];

        return view('pages.author.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Vytvorit autora',
            'description' => 'Vytvorenie autora',
            'routeUrl' => route('authors.store'),
            'formMethod' => null,
            'author' => new Author(),
        ];

        return view('pages.author.createOrEdit',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAuthor $request)
    {
        $author = Author::create($request->validated());
        $author->save();

        return redirect()->route('authors.index')->with('status', 'Autor bol vytvoreny');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'title' => 'Detail autora',
            'description' => 'Detaily autora',
            'author' => Author::with('books')->find($id),
        ];

        return view('pages.author.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'title' => 'Upravit autora',
            'description' => 'Uprava autora',
            'routeUrl' => route('authors.update',['author' => $id]),
            'formMethod' => '<input type="hidden" name="_method" value="PUT">',
            'author' => Author::find($id),
        ];

        return view('pages.author.createOrEdit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAuthor $request, $id)
    {
        if (!$author = Author::find($id)) {
            return redirect()->route('authors.index')->with('status', 'Autor sa nenasiel');
        }

        try {

            $author->update($request->validated());
            return redirect()->route('authors.index')->with('status', 'Autor bol upraveny');

        } catch (\Throwable $th) {

            return redirect()->route('authors.index')->with('status', 'Autora sa nepodarilo upravit');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$author = Author::find($id)) {
            return redirect()->route('authors.index')->with('status', 'Autor sa nenasiel');
        }

        try {

            $author->delete();
            return redirect()->route('authors.index')->with('status', 'Autor bol zmazany');

        } catch (\Throwable $th) {
            
            return redirect()->route('authors.index')->with('status', 'Autora sa nepodarilo vymazat');
        }

    }

    /**
     * Return sorted authors. 
     * @param Request $request 
     * @return Collection of Author
     */
    protected function sortedAuthors(Request $request) : Collection
    {
        $sortable = [
            'name',
            'surname',
            'count',
        ];

        $sort = $request->query('sort');
        $order = $request->query('order');

        if (!in_array($sort, $sortable)) {
            return Author::with('books')->get();
        }

        if ($order == 'asc' || $order == 'desc') {
            
            if ($sort == 'count') {
                return Author::with('books')->withCount('books')->orderBy('books_count', $order)->get();
            }

            return Author::with('books')->orderBy($sort, $order)->get();
        }
    }
}
