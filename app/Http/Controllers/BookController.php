<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Author;
use App\Http\Requests\StoreBook;
use App\Http\Requests\UpdateBook;
use Illuminate\Support\Collection;

class BookController extends Controller
{
    /**
     * Change state of specific book to 'borrowed' (is_borrowed = 1)
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lendBook($id)
    {
        if (!$book = Book::find($id)) {
            return redirect()->back()->with('status', 'Kniha sa nenasla');
        }

        if ($book->is_borrowed) {
            return redirect()->back()->with('status', 'Kniha je uz pozicana');
        }

        $book->is_borrowed = true;
        $book->save();
        
        return redirect()->back()->with('status', 'Kniha bola pozicana');
    }

    /**
     * Change state of specific book to 'returned' (is_borrowed = 0)
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function returnBook($id)
    {
        if (!$book = Book::find($id)) {
            return redirect()->back()->with('status', 'Kniha sa nenasla');
        }

        if (!$book->is_borrowed) {
            return redirect()->back()->with('status', 'Kniha je uz vratena');
        }

        $book->is_borrowed = false;
        $book->save();
        
        return redirect()->back()->with('status', 'Kniha bola vratena');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [
            'title' => 'Zoznam knih',
            'description' => 'Zoznam knih',
            'books' => $this->sortedBooks($request),
        ];

        return view('pages.book.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = [
            'title' => 'Vytvorit knihu',
            'description' => 'Vytvorenie knihu',
            'routeUrl' => route('books.store'),
            'formMethod' => null,
            'book' => new Book(),
            'authors' => Author::get(),
            'author_id' => $request->query('author_id'),
        ];

        return view('pages.book.createOrEdit',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBook $request)
    {
        $data = $request->validated();
        $author = Author::find($data['author_id']);

        $book = new Book;
        $book->title =  $data['title'];
        $book->is_borrowed = false;
        $book->author()->associate($author);
        $book->save();

        return redirect()->route('books.index')->with('status', 'Kniha bola vytvorena');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'title' => 'Detail knihy',
            'description' => 'Detaily knihy',
            'book' => Book::find($id),
        ];

        return view('pages.book.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'title' => 'Upravit knihu',
            'description' => 'Uprava knihu',
            'routeUrl' => route('books.update',['book' => $id]),
            'formMethod' => '<input type="hidden" name="_method" value="PUT">',
            'book' => Book::find($id),
            'authors' => Author::get(),
        ];

        return view('pages.book.createOrEdit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBook $request, $id)
    {
        if (!$book = Book::find($id)) {
            return redirect()->route('books.index')->with('status', 'Kniha sa nenasla');
        }

        try {

            $data = $request->validated();
            $author = Author::find($data['author_id']);

            $book->title =  $data['title'];
            $book->author()->associate($author);
            $book->save();
            return redirect()->route('books.index')->with('status', 'Kniha bola upravena');

        } catch (\Throwable $th) {

            return redirect()->route('books.index')->with('status', 'Knihu sa nepodarilo upravit');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!$book = Book::find($id)) {
            return redirect()->route('books.index')->with('status', 'Kniha sa nenasla');
        }

        try {

            $book->delete();
            return redirect()->route('books.index')->with('status', 'Kniha bola zmazana');

        } catch (\Throwable $th) {
            
            return redirect()->route('books.index')->with('status', 'Knihu sa nepodarilo vymazat');
        }

    }

    /**
     * Return sorted books. 
     * @param Request $request 
     * @return Collection of Book
     */
    protected function sortedBooks(Request $request) : Collection
    {
        $sortable = [
            'title',
            'author',
            'is_borrowed',
        ];

        $sort = $request->query('sort');
        $order = $request->query('order');

        if (!in_array($sort, $sortable)) {
            return Book::with('author')->get();
        }

        if ($order == 'asc' || $order == 'desc') {
            
            if ($sort == 'author') {
                return Book::with(['author' => function ($query) use ($order) {
                    $query->orderBy('name', $order);
                }])->get();
            }

            return Book::with('author')->orderBy($sort, $order)->get();
        }
    }
}