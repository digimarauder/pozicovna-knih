<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
    ];

    // RELATIONS

    
    public function books()
    {
        return $this->hasMany(Book::class, 'author_id', 'id');
    }

    // METHODS

    /**
     * Get count of all books that belongs to this author.
     * @return int;
     */
    public function getBooksCount() : int
    {
        return $this->books->count();
    }
    
    public function getFullNameAttribute() : string
    {
        return "{$this->name} {$this->surname}";
    }
}
