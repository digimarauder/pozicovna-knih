<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item @if($composerCurrentRouteName == 'dashboard') active @endif">
                <a class="nav-link" href="/">Dashboard <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item @if($composerCurrentRouteName == 'authors.index') active @endif">
                <a class="nav-link" href="{{route('authors.index')}}">Zoznam autorov</a>
            </li>
            <li class="nav-item @if($composerCurrentRouteName == 'books.index') active @endif">
                <a class="nav-link" href="{{route('books.index')}}">Zoznam knih</a>
            </li>
        </ul>
    </div>
</nav>
