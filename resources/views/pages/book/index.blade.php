@extends('layouts.default')

@section('title', $title)
@section('description', $description)

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <table class="table" id="indexTable">
        <caption>{{$title}}</caption>
        <tr>
            <th>
                Titul
                <a href="?sort=title&order=desc" title="zoradit vzostupne"><i class="fas fa-sort-up"></i></a>
                <a href="?sort=title&order=asc" title="zoradit zostupne"><i class="fas fa-sort-down"></i></a>
            </th>
            <th>
                Meno autora
                <a href="?sort=author&order=desc" title="zoradit vzostupne"><i class="fas fa-sort-up"></i></a>
                <a href="?sort=author&order=asc" title="zoradit zostupne"><i class="fas fa-sort-down"></i></a>
            </th>
            <th>
                Je zapozicana
                <a href="?sort=is_borrowed&order=desc" title="zoradit vzostupne"><i class="fas fa-sort-up"></i></a>
                <a href="?sort=is_borrowed&order=asc" title="zoradit zostupne"><i class="fas fa-sort-down"></i></a>
            </th>
            <th>
                Akcie
            </th>
        </tr>
        
        {{-- add new book action --}}
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <a href="{{route('books.create')}}">Pridat knihu</a>
            </td>
        </tr>

        {{-- list books --}}
        @foreach ($books as $book)
        <tr>
            <td>{{$book->title}}</td>
            <td><a href="{{route('authors.show',['author' => $book->author->id])}}">{{$book->author->fullName}}</a></td>
            <td>{{$book->is_borrowed ? 'Ano' : 'Nie'}}</td>
            <td>
                <a href="{{route('books.show', ['book' => $book->id])}}" title="zobrazit knihu"><i class="fas fa-eye"></i></a>

                &nbsp;

                <a href="{{route('books.edit', ['book' => $book->id])}}" title="upravit knihu"><i class="fas fa-edit"></i></a>

                &nbsp;

                @if ($book->is_borrowed)
                <a href="{{route('books.return',['book' => $book->id])}}" title="vratit knihu"><i class="fas fa-arrow-alt-circle-down"></i></a>
                @else
                <a href="{{route('books.lend',['book' => $book->id])}}" title="pozicat knihu"><i class="fas fa-arrow-alt-circle-up"></i></a>
                @endif

                &nbsp;
                
                <i class="fas fa-trash" data-url="{{route('books.destroy',['book' => $book->id])}}" title="zmazat knihu"></i>
            </td>
        </tr>
        @endforeach
    </table>
@stop

@push('scripts')
    <script>
        {{--  
            Add event listner on table element for book deletion.
        --}}
        (function() {
            function confirmDelete()
            {
                if ( !event.target.classList.contains("fa-trash") ) {
                    return;
                }

                const url = event.target.dataset.url;
                if (typeof url === "undefined") {
                    return;
                }

                if (confirm('Kniha bude vymazana. Pokracovat?')) {
                    const form = document.createElement("FORM");
                    form.action = url;
                    form.enctype = "application/x-www-form-urlencoded";
                    form.method = "post";
    
                    const deleteInputField = document.createElement("INPUT");
                    deleteInputField.setAttribute("type","hidden");
                    deleteInputField.name = "_method";
                    deleteInputField.value = "DELETE";
                    form.appendChild(deleteInputField);
    
                    const csrfTokenInputField = document.createElement("INPUT");
                    csrfTokenInputField.setAttribute("type","hidden");
                    csrfTokenInputField.name = "_token";
                    csrfTokenInputField.value = "{{csrf_token()}}";
                    form.appendChild(csrfTokenInputField);
    
                    document.body.appendChild(form);
                    form.submit();
                }
            }
            
            const table = document.getElementById("indexTable")
            table.addEventListener("click", confirmDelete, false);

        })();
       
    </script>
@endpush