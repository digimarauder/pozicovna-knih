@extends('layouts.default')

@section('title', $title)
@section('description', $description)

@section('content')
    @if($book)
        <h1>{{ $book->title }}</h1>
        
        <b>Stav:</b> {{ ($book->is_borrowed) ? 'Zapozicana' : 'Dostupna'}} <br/>
        <b>Autor:</b> {{ $book->author->fullName }} <br/>
        
    @else 
    <div class="alert alert-primary">
        Kniha sa nenasla
    </div>
    @endif
@stop