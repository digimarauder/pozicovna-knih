@extends('layouts.default')

@section('title', $title)
@section('description', $description)

@section('content')
    @if ($book)
        <form action="{{ $routeUrl }}" method="POST" enctype="application/x-www-form-urlencoded">
            <h1>{{ $title }}</h1>
            @csrf
            {!! $formMethod !!}
            <div class="form-group">
                <label for="title">Titul</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Titul knihy" value="{{old('title') ?? $book->title ?? ''}}">
                @if($errors->has('title'))
                    <small class="form-text alert alert-danger">{{ $errors->first('title') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label for="author">Autor knihy</label>
                <select class="form-control" id="author_id" name="author_id">
                    <option value="0">--vyberte--</option>
                    @foreach ($authors as $author)
                    <option value="{{$author->id}}" @if( (isset($author_id) && $author_id == $author->id) || old('author_id') == $author->id || optional($book->author)->id == $author->id) selected @endif >{{$author->fullName}}</option>
                    @endforeach
                </select>
                @if($errors->has('author_id'))
                    <small class="form-text alert alert-danger">{{ $errors->first('author_id') }}</small>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Ulozit knihu</button>
        </form>
    @else
        <div class="alert alert-primary">
            Kniha sa nenasla
        </div>
    @endif
@stop
