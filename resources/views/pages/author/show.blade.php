@extends('layouts.default')

@section('title', $title)
@section('description', $description)

@section('content')
    @if($author)
        <h1>{{$author->name}}&nbsp;{{$author->surname}}</h1>
        
        <h2>Zoznam knih:</h2>
        <ul>
            @foreach ($author->books as $book)
            <li><a href="{{route('books.show',['book' => $book->id])}}">{{$book->title}}</li>
            @endforeach
        </ul>
    @else 
    <div class="alert alert-primary">
        Autor sa nenasiel
    </div>
    @endif
@stop