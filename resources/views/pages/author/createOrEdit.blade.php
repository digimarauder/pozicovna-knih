@extends('layouts.default')

@section('title', $title)
@section('description', $description)

@section('content')
    @if ($author)
        <form action="{{ $routeUrl }}" method="POST" enctype="application/x-www-form-urlencoded">
            <h1>{{ $title }}</h1>
            @csrf
            {!! $formMethod !!}
            <div class="form-group">
                <label for="name">Meno</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Meno autora" value="{{old('name') ?? $author->name ?? ''}}">
                @if($errors->has('name'))
                    <small class="form-text alert alert-danger">{{ $errors->first('name') }}</small>
                @endif
            </div>
            <div class="form-group">
                <label for="surname">Priezvisko</label>
                <input type="text" class="form-control" id="surname" name="surname" placeholder="Priezvisko autora" value="{{ old('surname') ?? $author->surname ?? '' }}">
                @if($errors->has('surname'))
                    <small class="form-text alert alert-danger">{{ $errors->first('surname') }}</small>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Ulozit autora</button>
        </form>
    @else
        <div class="alert alert-primary">
            Autor sa nenasiel
        </div>
    @endif

@stop
