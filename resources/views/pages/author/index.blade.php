@extends('layouts.default')

@section('title', $title)
@section('description', $description)

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <table class="table" id="indexTable">
        <caption>{{$title}}</caption>
        <tr>
            <th>
                Meno
                <a href="?sort=name&order=desc" title="zoradit vzostupne"><i class="fas fa-sort-up"></i></a>
                <a href="?sort=name&order=asc" title="zoradit zostupne"><i class="fas fa-sort-down"></i></a>
            </th>
            <th>
                Priezvisko
                <a href="?sort=surname&order=desc" title="zoradit vzostupne"><i class="fas fa-sort-up"></i></a>
                <a href="?sort=surname&order=asc" title="zoradit zostupne"><i class="fas fa-sort-down"></i></a>
            </th>
            <th>
                Pocet knih
                <a href="?sort=count&order=desc" title="zoradit vzostupne"><i class="fas fa-sort-up"></i></a>
                <a href="?sort=count&order=asc" title="zoradit zostupne"><i class="fas fa-sort-down"></i></a>
            </th>
            <th>
                Akcie
            </th>
        </tr>
        
        {{-- add new author action --}}
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <a href="{{route('authors.create')}}">Pridat autora</a>
            </td>
        </tr>

        {{-- list authors --}}
        @foreach ($authors as $author)
        <tr>
            <td>{{$author->name}}</td>
            <td>{{$author->surname}}</td>
            <td>{{$author->getBooksCount()}}</td>
            <td>
                <a href="{{route('authors.show', ['author' => $author->id])}}" title="zobrazit autora"><i class="fas fa-eye"></i></a>

                &nbsp;

                <a href="{{route('authors.edit', ['author' => $author->id])}}" title="upravit autora"><i class="fas fa-edit"></i></a>

                &nbsp;

                <a href="{{route('books.create',['author_id' => $author->id])}}" title="pridat knihu"><i class="fas fa-plus"></i></a>

                &nbsp;
                
                <i class="fas fa-trash" data-url="{{route('authors.destroy',['author' => $author->id])}}" title="zmazat autora"></i>
            </td>
        </tr>
        @endforeach
    </table>
@stop

@push('scripts')
    <script>
        {{--  
            Add event listner on table element for author deletion.
        --}}
        (function() {
            function confirmDelete()
            {
                if ( !event.target.classList.contains("fa-trash") ) {
                    return;
                }

                const url = event.target.dataset.url;
                if (typeof url === "undefined") {
                    return;
                }

                if (confirm('Autor bude vymazany. Pokracovat?')) {
                    const form = document.createElement("FORM");
                    form.action = url;
                    form.enctype = "application/x-www-form-urlencoded";
                    form.method = "post";
    
                    const deleteInputField = document.createElement("INPUT");
                    deleteInputField.setAttribute("type","hidden");
                    deleteInputField.name = "_method";
                    deleteInputField.value = "DELETE";
                    form.appendChild(deleteInputField);
    
                    const csrfTokenInputField = document.createElement("INPUT");
                    csrfTokenInputField.setAttribute("type","hidden");
                    csrfTokenInputField.name = "_token";
                    csrfTokenInputField.value = "{{csrf_token()}}";
                    form.appendChild(csrfTokenInputField);
    
                    document.body.appendChild(form);
                    form.submit();
                }
            }
            
            const table = document.getElementById("indexTable")
            table.addEventListener("click", confirmDelete, false);

        })();
       
    </script>
@endpush