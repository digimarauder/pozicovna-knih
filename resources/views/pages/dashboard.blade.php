@extends('layouts.default')

@section('title', 'Dashboard')
@section('description', 'Dashboard')

@section('content')
    <ul>
        <li><a href="{{route('books.index')}}">Zobrazit zoznam knih</a></li>
        <li><a href="{{route('authors.index')}}">Zobrazit zoznam autorov</a></li>
        <li><a href="{{route('books.create')}}">Pridat knihu</a></li>
        <li><a href="{{route('authors.create')}}">Pridat autora</a></li>
    </ul>
@endsection