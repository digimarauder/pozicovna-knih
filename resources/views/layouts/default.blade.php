<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">

  <title>@yield('title')</title>
  <meta name="description" content="@yield('description')" />
  <meta name="author" content="Andrej" />

  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
  <link rel="stylesheet" href="{{mix('css/app.css')}}" />


</head>

<body>
  <div class="container">

    @include('components.navbar')

    @yield('content')
    
  </div>

  @stack('scripts')
</body>
</html>